package ru.osfb.tracking.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Tracking extends Activity implements TrackingService.StateObserver {
    private TrackingService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        bindService(new Intent(this, TrackingService.class), new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                service = ((TrackingService.LocalBinder) iBinder).getService();
                service.observe(Tracking.this);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                service = null;
            }
        }, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (service != null) service.unobserve();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (service != null) service.observe(this);
    }

    public void startTrackingService(View view) {
        if (service != null) try {
            service.start();
        } catch (MalformedURLException e) {
            Log.e(this.getClass().getCanonicalName(), "URL error", e);
            showError("Incorrect server URL");
        }
    }

    public void stopTrackingService(View view) {
        if (service != null) service.stop();
    }

    public void openSettings(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    public void scanQR(View view) {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (Exception e) {
            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
            startActivity(marketIntent);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                try {
                    JSONObject obj = new JSONObject(contents);
                    String key = obj.getString("key");
                    String url = obj.getString("url");
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(TrackingService.TRACKER_KEY_SETTING, key);
                    editor.putString(TrackingService.SERVER_URL_SETTING, url);
                    editor.commit();
                    showInfo("New settings successfully read from QR");
                } catch(JSONException e) {
                    showError("Incorrect settings data format");
                }
            } else {
                showError("Operation cancelled");
            }
        }
    }
    @Override
    public void onUpdate() {
        String serviceState;
        if (service.isStarted()) {
            serviceState = service.isSending() ? "Sending" : "Started";
        } else serviceState = "Stopped";
        setText(R.id.serviceStateText, "Service state: " + serviceState);
        setText(R.id.networkStateText, "Network: " + (service.isTcpNetworkAvailable() ? "Available" : "Unavailable"));
        setText(R.id.gpsStateText, "Fine location: " + (service.isFineLocationAvailable() ? "Available" : "Unavailable"));
        setText(R.id.queueSizeText, "Queue size: " + service.getQueueSize());
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        setText(R.id.lastEventTimeText, "Last event: " + df.format(service.lastEventTime()));
        setText(R.id.lastSentTimeText, "Last send: " + df.format(service.lastSendTime()));
    }

    private void setText(int textViewId, String text) {
        ((TextView) findViewById(textViewId)).setText(text);
    }

    private void showError(String msg) {
        new AlertDialog.Builder(this).setTitle("Error").setMessage(msg).setCancelable(true).show();
    }
    private void showInfo(String msg) {
        new AlertDialog.Builder(this).setTitle("Info").setMessage(msg).setCancelable(true).show();
    }
}
