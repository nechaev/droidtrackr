package ru.osfb.tracking.android;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Author: sgl
 * Date: 20.07.13
 */
public class SettingsActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
