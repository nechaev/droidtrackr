package ru.osfb.tracking.android;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.*;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.*;

/**
 * Author: sgl
 * Date: 19.07.13
 */
public class TrackingService extends Service {
    private static final int NOTIFICATION_ID = 1;
    private static final int MAX_EVENTS_IN_PACKET = 300;
    private static final int MIN_PACKET_INTERVAL = 15000;

    private static final int NETWORK_LOCATION_INTERVAL = 300000;
    private static final int PING_INTERVAL = 60000;

    public static final String TRACKER_KEY_SETTING = "trackerKey";
    public static final String SERVER_URL_SETTING = "serverUrl";

    private LocationListener locationListener;
    private LocationListener fallbackLocationListener;
    private BroadcastReceiver connectionListener;
    private ThreadPoolExecutor executor;

    private String trackerKey;
    private URL serverUrl;
    private ConcurrentLinkedQueue<JSONObject> eventQueue;
    private volatile long lastSendTime = 0;
    private volatile long lastEventTime = 0;
    private boolean isStarted = false;
    private boolean isTcpNetworkAvailable = false;
    private boolean isFineLocationAvailable = false;

    private StateObserver stateObserver = null;

    public class LocalBinder extends Binder {
        public TrackingService getService() {
            return TrackingService.this;
        }
    }

    public interface StateObserver {
        void onUpdate();
    }

    private LocationManager getLocationManager() {
        return (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    private ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public void start() throws MalformedURLException {
        if (!isStarted) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            serverUrl = new URL(prefs.getString(SERVER_URL_SETTING,""));
            trackerKey = prefs.getString(TRACKER_KEY_SETTING, "");
            startService(new Intent(this, this.getClass()));
            startForeground(NOTIFICATION_ID, buildNotification());
            Log.d(this.getClass().getCanonicalName(), "Starting tracking service");
            isFineLocationAvailable = false;
            getLocationManager().requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 100, fallbackLocationListener);

            getLocationManager().requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListener);
            isStarted = true;
            updateConnectionState();
            registerReceiver(connectionListener, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            fireStateUpdate();
        }
    }

    private Notification buildNotification() {
        Notification notification = new Notification(R.drawable.footprints, getText(R.string.app_name), System.currentTimeMillis());
        Intent intent = new Intent(this, Tracking.class);
        notification.setLatestEventInfo(this, getText(R.string.app_name), "", PendingIntent.getActivity(this, 0, intent, 0));
        return notification;
    }

    public void stop() {
        if (isStarted) {
            Log.d(this.getClass().getCanonicalName(), "Stopping tracking service");

            getLocationManager().removeUpdates(fallbackLocationListener);

            getLocationManager().removeUpdates(locationListener);
            stopForeground(true);
            stopSelf();
            isStarted = false;
            eventQueue.clear();
            unregisterReceiver(connectionListener);
            fireStateUpdate();
        }
    }

    public void updateConnectionState() {
        NetworkInfo networkInfo = getConnectivityManager().getActiveNetworkInfo();
        isTcpNetworkAvailable = networkInfo != null && networkInfo.isConnected();
        try {
            eventQueue.add(buildBoolMessage(EventTypes.NETWORK_AVAILABLE_STATE, isTcpNetworkAvailable));
            triggerSend();
        } catch (JSONException e) {
            Log.e(this.getClass().getCanonicalName(), "JSON error", e);
        }
    }

    public void observe(StateObserver observer) {
        this.stateObserver = observer;
        fireStateUpdate();
    }

    public void unobserve() {
        stateObserver = null;
    }

    private void fireStateUpdate() {
        if (stateObserver != null) stateObserver.onUpdate();
    }

    public boolean isStarted() {
        return isStarted;
    }

    public boolean isTcpNetworkAvailable() {
        return isTcpNetworkAvailable;
    }

    public boolean isFineLocationAvailable() {
        return isFineLocationAvailable;
    }

    public boolean isSending() {
        return executor.getActiveCount() > 0;
    }

    public int getQueueSize() {
        return eventQueue.size();
    }


    public Date lastEventTime() {
        return new Date(lastEventTime);
    }

    public Date lastSendTime() {
        return new Date(lastSendTime);
    }

    class TrackLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location.getProvider().equals(LocationManager.GPS_PROVIDER) && !isFineLocationAvailable) {
                fireStateUpdate();
                try {
                    eventQueue.add(buildBoolMessage(EventTypes.FINE_LOCATION_STATE, true));
                    triggerSend();
                } catch(JSONException e) {
                    Log.e(this.getClass().getCanonicalName(), "JSON error", e);
                }
            }
            sendNewLocation(location);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER) && status != LocationProvider.AVAILABLE) {
                fireStateUpdate();
                try {
                    eventQueue.add(buildBoolMessage(EventTypes.FINE_LOCATION_STATE, false));
                    triggerSend();
                } catch(JSONException e) {
                    Log.e(this.getClass().getCanonicalName(), "JSON error", e);
                }
            }
        }
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onProviderDisabled(String provider) {}
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locationListener = new TrackLocationListener();
        fallbackLocationListener = new TrackLocationListener();
        connectionListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateConnectionState();
                fireStateUpdate();
            }
        };
        executor = new ScheduledThreadPoolExecutor(5);
        eventQueue = new ConcurrentLinkedQueue<JSONObject>();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    private void sendNewLocation(Location location) {
        try {
            eventQueue.add(buildLocationMessage(location));
            lastEventTime = location.getTime();
            if (location.getTime() - lastSendTime > MIN_PACKET_INTERVAL) triggerSend();
        } catch (JSONException e) {
            Log.e(this.getClass().getCanonicalName(), "JSON error", e);
        } finally {
            fireStateUpdate();
        }
    }

    private void triggerSend() {
        if (isTcpNetworkAvailable && eventQueue.size() > 0 && executor.getActiveCount() == 0) try {
            Log.d(this.getClass().getCanonicalName(),"Sending new location to " + serverUrl.toString());

            executor.submit(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    HttpURLConnection conn = (HttpURLConnection) serverUrl.openConnection();
                    fireStateUpdate();
                    try {
                        conn.setConnectTimeout(60000);
                        conn.setReadTimeout(60000);
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type","application/json");
                        conn.setDoOutput(true);
                        OutputStream os = conn.getOutputStream();
                        OutputStreamWriter writer = new OutputStreamWriter(os);
                        writer.write("{\"k\":\"");
                        writer.write(trackerKey);
                        writer.write("\",\"e\":[");
                        int msgCount = 0;
                        for (JSONObject msg : eventQueue) {
                            if (msgCount > 0) writer.write(",");
                            writer.write(msg.toString());
                            if (msgCount++ > MAX_EVENTS_IN_PACKET) break;
                        }
                        writer.write("]}");
                        writer.flush();
                        writer.close();
                        os.flush();
                        os.close();
                        int response = conn.getResponseCode();
                        if (response == HttpURLConnection.HTTP_OK) {
                            Log.d(this.getClass().getCanonicalName(), "Messages sent: " + msgCount);
                            for (int i=0; i<msgCount; i++) lastSendTime = eventQueue.poll().getLong("t");
                            if (isTcpNetworkAvailable && eventQueue.size()>0) executor.submit(this);
                        } else {
                            Log.e(this.getClass().getCanonicalName(), "Response code: " + response + "\n" + conn.getResponseMessage());
                        }
                    } finally {
                        conn.disconnect();
                        fireStateUpdate();
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    private JSONObject buildLocationMessage(Location location) throws JSONException {
        JSONObject msg = new JSONObject();
        msg.put("t", location.getTime());
        msg.put("la", location.getLatitude());
        msg.put("lo", location.getLongitude());
        if (location.hasAltitude()) msg.put("al", location.getAltitude());
        if (location.hasSpeed()) msg.put("s", location.getSpeed() * 3.6); // m/s -> km/h
        if (location.hasBearing()) msg.put("h", location.getBearing());
        if (location.hasAccuracy()) msg.put("ac", location.getAccuracy());
        return msg;
    }
    private JSONObject buildBoolMessage(EventTypes eventType, boolean value) throws JSONException {
        JSONObject msg = new JSONObject();
        msg.put("t", new Date().getTime());
        msg.put("et", eventType.eventType);
        msg.put("bv", value);
        return msg;
    }
}
