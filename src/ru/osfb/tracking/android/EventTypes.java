package ru.osfb.tracking.android;

/**
 * Created by: sgl
 * Date: 14.10.13
 */
public enum EventTypes {
    MOTION_STATE(-1),
    FINE_LOCATION_STATE(-2),
    NETWORK_AVAILABLE_STATE(-3);

    public final int eventType;

    EventTypes(int code) {
        this.eventType = code;
    }


}
